import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { IMIRequest, IUserContext } from '@infor-up/m3-odin';
import { MIService, UserService } from '@infor-up/m3-odin-angular';
import { Observable } from 'rxjs';
import * as XLSX from 'xlsx';
import { map, startWith } from 'rxjs/operators';
import { ConfirmFinDialogComponent } from 'src/app/shared/components/dialogs/confirm-fin-dialog/confirm-fin-dialog.component';
import { ErrorAdviseLineDialogComponent } from 'src/app/shared/components/dialogs/error-advise-line-dialog/error-advise-line-dialog.component';
import { ErrorApiDialogComponent } from 'src/app/shared/components/dialogs/error-api-dialog/error-api-dialog.component';
import { SelectOptionsDialogComponent } from '../../shared/components/dialogs/select-options-dialog/select-options-dialog.component';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';


@Component({
  selector: 'app-saisie-attendus',
  templateUrl: './saisie-attendus.component.html',
  styleUrls: ['./saisie-attendus.component.scss']
})
export class SaisieAttendusComponent implements OnInit {

  model = {
    // partenaire: '',
    fournisseur: '',
    numeroBL: '',
    numeroColis: ''
  };

  userContext: IUserContext;

  // @ViewChild('partenaire') partenaireElement: ElementRef;
  @ViewChild('fournisseur') fournisseurElement: ElementRef;
  @ViewChild('numeroBL') numeroBLElement: ElementRef;
  @ViewChild('numeroColis') numeroColisElement: ElementRef;
  @ViewChildren('inputQuantite') inputElements: QueryList<ElementRef>;
  @ViewChild('articleFilterId') articleFilterElement: ElementRef;

  isLoading = false;

  fournisseurControl = new FormControl();
  filteredValuesFournisseur: Observable<any[]>;
  potentialValuesFournisseur: any[] = [];
  /*potentialValuesFournisseur: any[] = [
    {SUNO: 'FOURN001', SUNM: 'Fournisseur 1'},
    {SUNO: 'FOURN002', SUNM: 'Fournisseur 2'}
  ];*/

  ELEMENT_DATA: any[] = [
    { numeroOA: 'OA4632', article: 'Article 1', libelle: 'Libellé 1', reste: 10, quantite: 4 },
    { numeroOA: 'OA4633', article: 'Article 2', libelle: 'Libellé 2', reste: 5, quantite: 0 },
    { numeroOA: 'OA4634', article: 'Article 3', libelle: 'Libellé 3', reste: 15, quantite: 8 },
    { numeroOA: 'OA4635', article: 'Article 1', libelle: 'Libellé 1', reste: 8, quantite: 0 },
    { numeroOA: 'OA4635', article: 'Article 4', libelle: 'Libellé 4', reste: 20, quantite: 0 },
    { numeroOA: 'OA4636', article: 'Article 5', libelle: 'Libellé 5', reste: 2, quantite: 0 }
  ];

  @ViewChild('table') table: MatTable<any>;
  // dataSource = new MatTableDataSource();
  // dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  displayedColumns: string[] = ['numeroOA', 'article', 'lienFort', 'reste', 'quantite'];

  numeroOAFilter = new FormControl();
  articleFilter = new FormControl();
  libelleFilter = new FormControl();
  lienFortFilter = new FormControl();
  resteFilter = new FormControl();
  quantiteFilter = new FormControl();

  filteredValues = {
    numeroOA: '',
    article: '',
    libelle: '',
    lienFort: '',
    reste: '',
    quantite: ''
  };

  numeroBLIsBlocked = true;
  parametrageEmballage = '';

  dateSMDT = '';

  @ViewChild('inputOverlay') inputOverlay: ElementRef;

  private transformer = (node: any, level: number) => {
    return {
      expandable: node['V_LFCD'] === '1' && node.lienFort === '*',
      numeroOA: node.numeroOA,
      isChild: node.isChild,
      isFiltered: node.isFiltered,
      article: node.article,
      libelle: node.libelle,
      lienFort: node.lienFort,
      reste: node.reste,
      quantite: node.quantite,
      level: level,
    };
  }

  treeControl = new FlatTreeControl<any>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    this.transformer, node => node.level,
    node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  expandedNodes: Array<any> = [];

  constructor(private miService: MIService, public dialog: MatDialog, private userService: UserService) { }

  async ngOnInit(): Promise<void> {

    this.isLoading = true;
    setTimeout(() => {
      this.inputOverlay.nativeElement.focus();
    }, 100);

    this.fournisseurControl.setValue('');
    this.fournisseurControl.disable();
    // this.fournisseurControl = new FormControl({ value: '', disabled: true });

    this.userService.getUserContext().subscribe(async userContext => {
      this.userContext = userContext;

      this.numeroOAFilter.valueChanges.subscribe((numeroOAFilterValue) => {
        this.filteredValues['numeroOA'] = numeroOAFilterValue;

        this.filtrageData();
        // this.dataSource.filter = JSON.stringify(this.filteredValues);
      });

      this.articleFilter.valueChanges.subscribe((articleFilterValue) => {
        this.filteredValues['article'] = articleFilterValue;

        this.filtrageData();
        // this.dataSource.filter = JSON.stringify(this.filteredValues);
      });

      this.libelleFilter.valueChanges.subscribe((libelleFilterValue) => {
        this.filteredValues['libelle'] = libelleFilterValue;

        this.filtrageData();
        // this.dataSource.filter = JSON.stringify(this.filteredValues);
      });

      this.lienFortFilter.valueChanges.subscribe((lienFortFilterValue) => {
        this.filteredValues['lienFort'] = lienFortFilterValue;

        this.filtrageData();
        // this.dataSource.filter = JSON.stringify(this.filteredValues);
      });

      this.resteFilter.valueChanges.subscribe((resteFilterValue) => {
        this.filteredValues['reste'] = resteFilterValue;

        this.filtrageData();
        // this.dataSource.filter = JSON.stringify(this.filteredValues);
      });

      this.quantiteFilter.valueChanges.subscribe((quantiteFilterValue) => {
        this.filteredValues['quantite'] = quantiteFilterValue;

        this.filtrageData();
        // this.dataSource.filter = JSON.stringify(this.filteredValues);
      });

      // this.dataSource.filterPredicate = this.customFilterPredicate();

      await this.loadFournisseur();

      this.filteredValuesFournisseur = this.fournisseurControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filterFournisseur(value))
      );

      const czztabResponse = await this.selectEXPORTMICZZTAB();
      if (czztabResponse.errorCode !== undefined) {

      }

      this.parametrageEmballage = czztabResponse.item['REPL'];
      this.isLoading = false;

    });
  }

  private _filterFournisseur(value: string): string[] {
    const filterValue = value.toLowerCase();
    if (filterValue.length <= 3) {
      return [];
    }
    return this.potentialValuesFournisseur.filter(option =>
      option['SUNO'].toLowerCase().includes(filterValue) || option['SUNM'].toLowerCase().includes(filterValue));
  }

  filtrageData() {
    this.dataSource._flattenedData.value.forEach((data: any) => {

      if (data.numeroOA.toString().trim().toLowerCase().indexOf(this.filteredValues.numeroOA.toLowerCase()) !== -1) {
        if (data.article.toString().trim().toLowerCase().indexOf(this.filteredValues.article.toLowerCase()) !== -1) {
          if (data.lienFort.toString().trim().toLowerCase().indexOf(this.filteredValues.lienFort.toLowerCase()) !== -1) {
            if (!!this.filteredValues.quantite) {
              if (this.filteredValues.quantite.startsWith('>')) {
                if (this.filteredValues.quantite.startsWith('>=')) {
                  data.isFiltered = !(data.quantite >= parseFloat(this.filteredValues.quantite.substr(2)));
                  return;
                }
                data.isFiltered = !(data.quantite > parseFloat(this.filteredValues.quantite.substr(1)));
                return;
              } else if (this.filteredValues.quantite.startsWith('<')) {
                if (this.filteredValues.quantite.startsWith('<=')) {
                  data.isFiltered = !(data.quantite <= parseFloat(this.filteredValues.quantite.substr(2)));
                  return;
                }
                data.isFiltered = !(data.quantite < parseFloat(this.filteredValues.quantite.substr(1)));
                return;
              } else {
                data.isFiltered = !(data.quantite === parseFloat(this.filteredValues.quantite));
                return;
              }
            }
            data.isFiltered = false;
            return;
          }
        }
      }
      data.isFiltered = true;
    });
  }

  createExcel() {
    let data = JSON.parse(JSON.stringify(this.dataSource.data));
    let dataToExcel = data.map(item => {
      let newItem = {};
      newItem['Numero fournisseur'] = this.model.fournisseur;
      newItem['Numero OA'] = item.numeroOA;
      newItem['Article'] = item.article;
      newItem['Lien Fort'] = item.lienFort;
      newItem['Reste à aviser'] = item.reste;
      newItem['Quantité BL / colis'] = item.quantite;

      return newItem;
    })
    console.log(dataToExcel);
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(dataToExcel);
    console.log('WS', ws);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Extraction fournisseur ' + this.model.fournisseur);


    /* save to file */
    XLSX.writeFile(wb, `Saisie_Attendus_OAs_${this.getDate()}_${this.getTime()}.xlsx`);
  }

  getDate() {
    let date = new Date();
    let day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    let month = date.getMonth() + 1 < 10 ? "0" + date.getMonth() : date.getMonth();
    let year = date.getFullYear();
    return day + "-" + month + "-" + year;
  }

  getTime() {
    let date = new Date();
    let hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
    let minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    let seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
    return hours + ":" + minutes + ":" + seconds;
  }

  async loadFournisseur() {

    const lstSuppliersResponse = await this.lstSuppliersCRS620MI();
    if (lstSuppliersResponse.errorCode !== undefined) {
      console.log(lstSuppliersResponse.errorMessage);
      return;
    }

    this.potentialValuesFournisseur = lstSuppliersResponse.items;
    this.fournisseurControl.setValue('');
    this.fournisseurControl.enable();
    this.numeroBLIsBlocked = false;
    // this.fournisseurControl = new FormControl({ value: '', disabled: false });
  }


  customFilterPredicate() {
    const myFilterPredicate = (data: any, filter: string): boolean => {

      let searchString = JSON.parse(filter);
      /*console.log(searchString);
      console.log(data);
      console.log('indexOf numeroOA', data.numeroOA.toString().trim().toLowerCase().indexOf(searchString.numeroOA.toLowerCase()));
      console.log('indexOf article', data.article.toString().trim().toLowerCase().indexOf(searchString.article.toLowerCase()));*/
      if (data.numeroOA.toString().trim().toLowerCase().indexOf(searchString.numeroOA.toLowerCase()) !== -1) {
        if (data.article.toString().trim().toLowerCase().indexOf(searchString.article.toLowerCase()) !== -1) {
          if (!!searchString.quantite) {
            debugger;
            if (searchString.quantite.startsWith('>')) {
              if (searchString.quantite.startsWith('>=')) {
                return data.quantite >= parseFloat(searchString.quantite.substr(2));
              }
              return data.quantite > parseFloat(searchString.quantite.substr(1));
            } else if (searchString.quantite.startsWith('<')) {
              if (searchString.quantite.startsWith('<=')) {
                return data.quantite <= parseFloat(searchString.quantite.substr(2));
              }
              return data.quantite < parseFloat(searchString.quantite.substr(1));
            } else {
              return data.quantite === parseFloat(searchString.quantite);
            }
          }
          return true;
        }
      }
      return false;
      /*return data.position.toString().trim().indexOf(searchString.position) !== -1 &&
        data.name.toString().trim().toLowerCase().indexOf(searchString.name.toLowerCase()) !== -1;*/
    };
    return myFilterPredicate;
  }

  getTotalQuantite() {
    return this.dataSource._flattenedData.value.map(item => { return item.isChild || item.isFiltered ? 0 : item['quantite']; }).reduce((acc, value) => parseFloat(acc) + parseFloat(value), 0);
  }

  isButtonValiderDisabled() {
    return this.areQuantiteSuperiorToReste() || !this.hasBeenLoaded();
  }

  hasBeenLoaded() {
    return this.dataSource.data.length !== 0;
  }

  listenToChangeOnQuantity(element, index, event) {

    console.log(element, index, event);


    if (element.isChild) {
      const parentIndex = this.dataSource.data.findIndex(item => item.numeroOA === element.numeroOA && item.article === element.article && item.lienFort === '*');
      console.log(parentIndex);
      console.log(this.dataSource.data[parentIndex]);


      const childIndex = this.dataSource.data[parentIndex].children.findIndex(item => item.numeroOA === element.numeroOA && item.article === element.article && item.lienFort === element.lienFort)

      this.dataSource.data[parentIndex].children[childIndex].quantite = element.quantite;

      this.dataSource.data[parentIndex].quantite = this.dataSource.data[parentIndex].children
        .map(item => item['quantite'])
        .reduce((acc, value) => parseFloat(acc) + parseFloat(value), 0);

    } else {
      const parentIndex = this.dataSource.data.findIndex(item => item.numeroOA === element.numeroOA && item.article === element.article && item.lienFort === element.lienFort);
      this.dataSource.data[parentIndex].quantite = element.quantite;
    }

    /*if (element.isChild) {
      setTimeout(() => {
        this.dataSource._expandedData.subscribe(items => {
          const parent = items.filter(item => item.numeroOA === element.numeroOA && item.article === element.article && item.lienFort === '*')[0];

          parent.quantite = items.filter(item => item.numeroOA === element.numeroOA && item.article === element.article && item.lienFort !== '*')
            .map(item => item['quantite'])
            .reduce((acc, value) => parseFloat(acc) + parseFloat(value), 0);

        });

      }, 100);
    }*/

    if (element.quantite === null) {
      element.quantite = 0;
    }

    // this.dataSource.data = JSON.parse(JSON.stringify(this.dataSource.data));

  }

  focusOutQuantity(element, index, event) {
    console.log('focusOutQuantity');
    this.saveExpandedNodes();
    this.dataSource.data = JSON.parse(JSON.stringify(this.dataSource.data));
    this.restoreExpandedNodes();
  }

  saveExpandedNodes() {
    this.expandedNodes = new Array<any>();
    this.treeControl.dataNodes.forEach(node => {
      if (node.expandable && this.treeControl.isExpanded(node)) {
        this.expandedNodes.push(node);
      }
    });
  }

  restoreExpandedNodes() {
    this.expandedNodes.forEach(node => {
      this.treeControl.expand(this.treeControl.dataNodes.find(n => n.numeroOA === node.numeroOA && n.article == node.article && n.lienFort == node.lienFort));
    });
  }

  getAncestors(array, element) {
    if (typeof array != "undefined") {
      for (let i = 0; i < array.length; i++) {
        if (array[i].numeroOA === element.numeroOA && array[i].article === element.article && array[i].lienFort === element.lienFort) {
          if (array[i].isChild === true) {

          }
          return [i];
        }
        const a = this.getAncestors(array[i].children, element);
        if (a !== null) {
          a.unshift(i);
          return a;
        }
      }
    }
    return null;
  }

  removeInvalidChar(event) {
    const invalidChars = ['-', '+', 'e'];
    if (invalidChars.includes(event.key)) {
      event.preventDefault();
    }
  }

  isQuantiteOverReste(quantite, reste) {
    return quantite > reste;
  }

  areQuantiteSuperiorToReste() {
    return this.dataSource.data.some(item => item['quantite'] > item['reste']);
  }

  onEnterQuantite() {
    this.articleFilterElement.nativeElement.focus();
  }

  onEnterArticleFilter() {
    this.inputElements.first.nativeElement.focus();
  }

  async openModal() {
    console.log(this.dataSource.data);
    debugger;

    if (this.checkFields()) {

      const isNumColisEmpty = !this.model.numeroColis;

      if (this.getTotalQuantite() !== 0) {
        if (isNumColisEmpty) {
          this.handleError('Le numéro de colis doit être renseigné');
          this.numeroColisElement.nativeElement.focus();
          return false;
        }
      }

      const dialogRef = this.dialog.open(SelectOptionsDialogComponent, {
        data: {
          quantiteVide: this.getTotalQuantite() === 0
        }
      });

      dialogRef.afterClosed().subscribe(async result => {
        if (result === 'bl') {
          await this.openModalConfirmCloture();
        }

        if (result === 'colis') {
          await this.processColis();
        }
      });
    }
  }

  async openModalConfirmCloture() {
    const dialogRef = this.dialog.open(ConfirmFinDialogComponent);
    dialogRef.afterClosed().subscribe(async result => {
      if (result === 'oui') {
        await this.processClotureBL();
        // Après traitement
        // Rendre le champs numéro fournisseur enabled
        this.fournisseurControl.setValue('');
        this.fournisseurControl.enable();
        // this.fournisseurControl = new FormControl({ value: '', disabled: false });
        // Remettre le champs numéro BL à blanc
        this.model.numeroBL = '';
        this.model.numeroColis = '';
      }
    });
  }

  async processColis() {
    this.isLoading = true;
    setTimeout(() => {
      this.inputOverlay.nativeElement.focus();
    }, 100);

    // Si ajout colis, disabled le champs numéro fournisseur
    this.fournisseurControl.disable();
    // this.fournisseurControl = new FormControl({ value: this.model.fournisseur, disabled: true });
    // Rendre le numéro BL disabled
    this.numeroBLIsBlocked = true;


    const date = new Date();
    let day: any = date.getDate();
    if (day < 10) { day = '0' + day; }
    let month: any = date.getMonth();
    if (month + 1 < 10) { month = '0' + (month + 1); }
    const year = date.getFullYear();
    const ymd8 = year + '' + month + '' + day;
    if (this.dateSMDT === '') {
      this.dateSMDT = ymd8;
    }

    // API flag blocage
    const flagBlocageResp = await this.addAlphaKPICUSEXTMI({
      CONO: this.userContext.currentCompany,
      KPID: 'SDK_WLOG01',
      PK01: 'FLAG_BLOCAGE_BL',
      PK02: this.model.numeroBL,
      PK03: this.model.fournisseur,
      AL30: '1',
      AL31: this.dateSMDT,
      AL39: 'Blocage Manhattan MHFRNS',
    });

    if (flagBlocageResp.errorCode !== undefined && flagBlocageResp.errorCode !== 'XRE0104') {
      this.handleError(flagBlocageResp);
      this.isLoading = false;
      return;
    }

    const normalLines = this.dataSource.data.filter(item => item['V_LFCD'] !== '1' && item['quantite'] > 0);

    // API Mise à jour : CMS100MI/LstWLOG01_11 puis PPS001MI/AdviseLine
    const uniqueOA = new Set(normalLines.map(e => e['numeroOA']));

    console.log('uniqueOA', uniqueOA);
    const dateMap = new Map();
    for (const oa of uniqueOA) {
      const listArticles = normalLines.filter(item => item['numeroOA'] === oa).map(item => item['article']);
      const res = await this.lstWLOG01_11CMS100MI({ SUNO: this.model.fournisseur, PUNO: oa });
      if (res.errorMessage !== undefined) {
        this.handleError(res.errorMessage);
      } else {
        console.log('Retour WLOG01_11', res);

        // TODO Ajout comportement sur V_LFCD === 1 
        for (const article of listArticles) {
          // tslint:disable-next-line:max-line-length
          const resFiltered = res.items.filter((item) => item.IBSUNO === this.model.fournisseur && item.IBPUNO === oa && item.IBITNO === article && item['V_LFCD'] !== '1');
          // tslint:disable-next-line:max-line-length
          // const resSorted = resFiltered.sort((a, b) => a.IBSUNO.localeCompare(b.IBSUNO) || a.IBPUNO.localeCompare(b.IBPUNO) ||  a.IBITNO.localeCompare(b.IBITNO) || a.IBPNLI.localeCompare(b.IBPNLI) || a.IBPNLS.localeCompare(b.IBPNLS) || a.V_DATE.localeCompare(b.V_DATE));
          const resSorted = resFiltered.sort((a, b) => parseInt(a['V_DATE'], 10) - parseInt(b['V_DATE'], 10));
          dateMap.set(`${oa};${article}`, resSorted);
          console.log(resSorted);
        }
      }
    }
    console.log('DateMap: ', dateMap);

    let anErrorHasOccured = false;
    const errors = [];
    const debutTimeStamp = Date.now();
    console.log('Map DATE : ', dateMap);
    for (const element of normalLines) {
      // await this.dataSource.data.forEach(async element => {
      if (element['quantite'] !== 0) {

        let quantiteSaisie = parseFloat(element['quantite']);
        const items = dateMap.get(`${element['numeroOA']};${element['article']}`);

        let obj = {};
        for (const item of items) {
          if (quantiteSaisie === 0) {
            break;
          }

          if (quantiteSaisie <= item['V_QRAL']) {
            obj = {
              PUNO: element['numeroOA'],
              PNLI: item['IBPNLI'],
              PNLS: item['IBPNLS'],
              SUNO: this.model.fournisseur,
              SUDO: this.model.numeroBL,
              ADQA: quantiteSaisie,
              PACT: this.parametrageEmballage,
              PACN: this.model.numeroColis
            };

            quantiteSaisie = 0;
          } else { // Si quantiteSaisie > item['V_QRAL']
            obj = {
              PUNO: element['numeroOA'],
              PNLI: item['IBPNLI'],
              PNLS: item['IBPNLS'],
              SUNO: this.model.fournisseur,
              SUDO: this.model.numeroBL,
              ADQA: item['V_QRAL'],
              PACT: this.parametrageEmballage,
              PACN: this.model.numeroColis
            };
            quantiteSaisie = quantiteSaisie - parseFloat(item['V_QRAL']);
          }
          console.log('AdviseLine OBJECT :', obj);
          const adviseLineResponse = await this.adviseLinePPS001MI(obj);

          if (adviseLineResponse.errorCode !== undefined) {
            const registerErrorResponse = await this.addAlphaKPICUSEXTMI({
              CONO: this.userContext.currentCompany,
              KPID: 'SDK_WLOG01',
              PK01: 'ERREUR',
              PK02: 'PPS001MI/AdviseLine',
              PK03: this.model.fournisseur,
              PK04: this.model.numeroBL,
              PK05: this.model.numeroColis,
              PK06: `${element['numeroOA']} ${item['IBPNLI']} ${item['IBPNLS']}`,
              PK07: debutTimeStamp,
              AL30: adviseLineResponse.errorCode,
              AL31: adviseLineResponse.errorMessage.substring(0, 30),
              AL32: adviseLineResponse.errorMessage.substring(30),
              AL33: element['article'],
              AL34: element['quantite'],
              AL39: 'Log erreur API'
            });
            if (registerErrorResponse.errorCode !== undefined) {
              console.log(registerErrorResponse);
            }
            anErrorHasOccured = true;
            errors.push({ ITNO: element['article'], ADQA: element['quantite'], PUNO: element['numeroOA'], PNLI: item['IBPNLI'], PNLS: item['IBPNLS'] });
          }
        }

      }

    }

    const otherLines = this.dataSource.data.filter(item => item['V_LFCD'] === '1' && item.quantite > 0);

    for (const line of otherLines) {
      if (line.lienFort === '*') { // Ligne dont le lien fort est *, donc possédant des lignes filles

        for (const child of line.children.filter(child => child.quantite > 0)) {
          let quantiteSaisie = parseFloat(child['quantite']);
          const obj = {
            PUNO: child['numeroOA'],
            PNLI: child['IBPNLI'],
            PNLS: child['IBPNLS'],
            SUNO: this.model.fournisseur,
            SUDO: this.model.numeroBL,
            ADQA: quantiteSaisie,
            PACT: this.parametrageEmballage,
            PACN: this.model.numeroColis
          };

          const adviseLineResponse = await this.adviseLinePPS001MI(obj);

          if (adviseLineResponse.errorCode !== undefined) {
            const registerErrorResponse = await this.addAlphaKPICUSEXTMI({
              CONO: this.userContext.currentCompany,
              KPID: 'SDK_WLOG01',
              PK01: 'ERREUR',
              PK02: 'PPS001MI/AdviseLine',
              PK03: this.model.fournisseur,
              PK04: this.model.numeroBL,
              PK05: this.model.numeroColis,
              PK06: `${child['numeroOA']} ${child['IBPNLI']} ${child['IBPNLS']}`,
              PK07: debutTimeStamp,
              AL30: adviseLineResponse.errorCode,
              AL31: adviseLineResponse.errorMessage.substring(0, 30),
              AL32: adviseLineResponse.errorMessage.substring(30),
              AL33: child['article'],
              AL34: child['quantite'],
              AL39: 'Log erreur API'
            });
            if (registerErrorResponse.errorCode !== undefined) {
              console.log(registerErrorResponse);
            }
            anErrorHasOccured = true;
            errors.push({ ITNO: child['article'], ADQA: child['quantite'], PUNO: child['numeroOA'], PNLI: child['IBPNLI'], PNLS: child['IBPNLS'] });
          }
        }

      } else { // Ligne dont le lien fort est défini
        let quantiteSaisie = parseFloat(line['quantite']);

        const obj = {
          PUNO: line['numeroOA'],
          PNLI: line['IBPNLI'],
          PNLS: line['IBPNLS'],
          SUNO: this.model.fournisseur,
          SUDO: this.model.numeroBL,
          ADQA: quantiteSaisie,
          PACT: this.parametrageEmballage,
          PACN: this.model.numeroColis
        };

        const adviseLineResponse = await this.adviseLinePPS001MI(obj);

        if (adviseLineResponse.errorCode !== undefined) {
          const registerErrorResponse = await this.addAlphaKPICUSEXTMI({
            CONO: this.userContext.currentCompany,
            KPID: 'SDK_WLOG01',
            PK01: 'ERREUR',
            PK02: 'PPS001MI/AdviseLine',
            PK03: this.model.fournisseur,
            PK04: this.model.numeroBL,
            PK05: this.model.numeroColis,
            PK06: `${line['numeroOA']} ${line['IBPNLI']} ${line['IBPNLS']}`,
            PK07: debutTimeStamp,
            AL30: adviseLineResponse.errorCode,
            AL31: adviseLineResponse.errorMessage.substring(0, 30),
            AL32: adviseLineResponse.errorMessage.substring(30),
            AL33: line['article'],
            AL34: line['quantite'],
            AL39: 'Log erreur API'
          });
          if (registerErrorResponse.errorCode !== undefined) {
            console.log(registerErrorResponse);
          }
          anErrorHasOccured = true;
          errors.push({ ITNO: line['article'], ADQA: line['quantite'], PUNO: line['numeroOA'], PNLI: line['IBPNLI'], PNLS: line['IBPNLS'] });
        }

      }
    }


    await this.loadData();

    if (anErrorHasOccured) {
      const dialogRef = this.handleErrorAdviseLine(errors);
      dialogRef.afterClosed().subscribe(() => {
        this.numeroBLIsBlocked = true;
        this.model.numeroColis = '';
        this.numeroColisElement.nativeElement.focus();
      });
    } else {
      this.numeroBLIsBlocked = true;
      this.model.numeroColis = '';
      this.numeroColisElement.nativeElement.focus();
    }
    this.isLoading = false;
  }

  checkFields() {

    /*const isPartenaireEmpty = !this.model.partenaire;
    if (isPartenaireEmpty) {
      this.handleError('Le partenaire doit être renseigné');
      this.partenaireElement.nativeElement.focus();
      return false;
    }
    */

    const isFournisseurEmpty = !this.model.fournisseur;
    if (isFournisseurEmpty) {
      this.handleError('Le numéro de fournisseur doit être renseigné');
      this.fournisseurElement.nativeElement.focus();
      return false;
    }

    const isNumBLEmpty = !this.model.numeroBL;
    if (isNumBLEmpty) {
      this.handleError('Le numéro de BL doit être renseigné');
      this.numeroBLElement.nativeElement.focus();
      return false;
    }

    return true;
  }

  async processClotureBL() {
    this.isLoading = true;
    setTimeout(() => {
      this.inputOverlay.nativeElement.focus();
    }, 100);

    if (this.getTotalQuantite() !== 0) {
      await this.processColis();
    }

    const delFlagResponse = await this.delAlphaKPICUSEXTMI({
      CONO: this.userContext.currentCompany,
      KPID: 'SDK_WLOG01',
      PK01: 'FLAG_BLOCAGE_BL',
      PK02: this.model.numeroBL,
      PK03: this.model.fournisseur
    });

    if (delFlagResponse.errorCode !== undefined) {
      console.log(delFlagResponse);
    }

    // CMS100MI / LstWLOG01_02

    const lstWlogResponse = await this.lstWLOG01_02CMS100MI({ USD1: this.model.numeroBL });
    if (lstWlogResponse.errorCode !== undefined) {
      // TODO Error
      this.handleError(lstWlogResponse.errorMessage);
      this.isLoading = false;
      return;
    }

    const element = lstWlogResponse.items.filter(item => item.J2USD1 === this.model.numeroBL && item.IASUNO === this.model.fournisseur)[0];


    // ZM3INTMI / ASNDecharg

    const debutTimeStamp = Date.now();
    const dechargResponse = await this.ASNdechargZM3INTMI({
      E0PA: element['J2E0PA'],
      USD1: this.model.numeroBL,
      DLIX: element['J2DLIX']
    });

    if (dechargResponse.errorCode !== undefined) {
      // TODO Error
      this.handleError(dechargResponse.errorMessage);
      const registerErrorResponse = await this.addAlphaKPICUSEXTMI({
        CONO: this.userContext.currentCompany,
        KPID: 'SDK_WLOG01',
        PK01: 'ERREUR',
        PK02: 'ZM3INTMI/ASNdecharg',
        PK03: this.model.fournisseur,
        PK04: this.model.numeroBL,
        PK05: debutTimeStamp,
        AL30: dechargResponse.errorCode,
        AL31: dechargResponse.errorMessage.substring(0, 30),
        AL32: dechargResponse.errorMessage.substring(30),
        AL39: 'Log erreur API'
      });
      if (registerErrorResponse.errorCode !== undefined) {
        console.log(registerErrorResponse);
      }
    }

    setTimeout(() => {
      this.numeroOAFilter.setValue('');
      this.articleFilter.setValue('');
      this.quantiteFilter.setValue('');
      this.dataSource.data = [];
      this.table.renderRows();
      this.fournisseurElement.nativeElement.focus();
      this.numeroBLIsBlocked = false;
      this.isLoading = false;

    }, 100);


  }


  /* async onEnterPartenaire() {
    // this.fournisseurElement.nativeElement.focus();
    this.numeroBLIsBlocked = false;
    const lstWlogResponse = await this.lstWLOG01_02CMS100MI({ USD1: this.model.numeroBL });
    if (lstWlogResponse.errorCode !== undefined) {
      // TODO Error
      this.handleError(lstWlogResponse.errorMessage);
      return;
    }

    if (lstWlogResponse.items.length !== 0) {
      // TODO Error
      this.handleError('Numéro de BL déjà saisi');
      return;
    }

    this.model.numeroColis = '1';

    this.loadData();
  }*/

  async onFocusOutFournisseur() {

    this.model.fournisseur = this.model.fournisseur.replace(/ +$/, '');
    this.model.numeroBL = this.model.numeroBL.replace(/ +$/, '');
    this.model.numeroColis = this.model.numeroColis.replace(/ +$/, '');
    this.dateSMDT = '';

    if (!!this.model.fournisseur) {
      this.isLoading = true;
      setTimeout(() => {
        this.inputOverlay.nativeElement.focus();
      }, 100);

      setTimeout(async () => {

        if (this.potentialValuesFournisseur.some(item => item.SUNO === this.model.fournisseur)) {
          this.loadData();

          const isNumColisEmpty = !this.model.numeroColis;

          if (isNumColisEmpty) {
            this.model.numeroColis = '1';
          }

          if (!this.model.numeroBL) {
            setTimeout(() => {
              this.numeroBLElement.nativeElement.focus();
            }, 100);
          } else {
            const lstWlogResponse = await this.lstWLOG01_02CMS100MI({ USD1: this.model.numeroBL });
            if (lstWlogResponse.errorCode !== undefined) {
              this.handleError(lstWlogResponse.errorMessage);
              this.isLoading = false;
              return;
            }

            // tslint:disable-next-line:max-line-length
            const itemsToCheck = lstWlogResponse.items.filter(item => item.IASUNO === this.model.fournisseur && item.J2USD1 === this.model.numeroBL);

            if (itemsToCheck.length !== 0) {
              if (!itemsToCheck.some(e => e.F3A030 === '1')) {
                this.handleError('Numéro de BL déjà saisi');
                this.isLoading = false;
                return;
              }
              this.dateSMDT = itemsToCheck[0].F3A130;

            }
          }

        } else {
          this.handleError('Le fournisseur saisi n\'existe pas.');
          this.isLoading = false;
          return;
        }
      }, 200);
    } else {
      this.dataSource.data = [];
      this.table.renderRows();
    }

  }

  async onEnterNumeroBL() {

    if (!!this.model.fournisseur && !!this.model.numeroBL) {
      this.model.fournisseur = this.model.fournisseur.replace(/ +$/, '');
      this.model.numeroBL = this.model.numeroBL.replace(/ +$/, '');
      this.model.numeroColis = this.model.numeroColis.replace(/ +$/, '');
      this.dateSMDT = '';
      this.isLoading = true;
      setTimeout(() => {
        this.inputOverlay.nativeElement.focus();
      }, 100);

      const lstWlogResponse = await this.lstWLOG01_02CMS100MI({ USD1: this.model.numeroBL });
      if (lstWlogResponse.errorCode !== undefined) {
        this.handleError(lstWlogResponse.errorMessage);
        this.isLoading = false;
        return;
      }

      // tslint:disable-next-line:max-line-length
      const itemsToCheck = lstWlogResponse.items.filter(item => item.IASUNO === this.model.fournisseur && item.J2USD1 === this.model.numeroBL);

      if (itemsToCheck.length !== 0) {
        if (!itemsToCheck.some(e => e.F3A030 === '1')) {
          this.handleError('Numéro de BL déjà saisi');
          this.isLoading = false;
          return;
        }
        this.dateSMDT = itemsToCheck[0].F3A130;

      }


      this.isLoading = false;

    }

    // this.onEnterFournisseur();
  }

  onEnterNumeroColis() {
    // this.onEnterFournisseur();
  }

  async loadData() {
    /*const lstWLOG03Response = await this.lstWLOG01_03CMS100MI({ E0PA: this.model.partenaire });
    if (lstWLOG03Response.errorCode !== undefined) {
      // TODO Error
      this.handleError(lstWLOG03Response.errorMessage);
      return;
    }

    const firstItem = lstWLOG03Response.items[0];
    const lastItem = lstWLOG03Response.items[lstWLOG03Response.items.length - 1];*/

    this.dataSource.data = [];
    this.table.renderRows();

    const lstWLOG01Response = await this.lstWLOG01_01CMS100MI({
      /*F_WHLO: firstItem['I7WHLO'],
      T_WHLO: lastItem['I7WHLO'],*/
      SUNO: this.model.fournisseur
    });

    if (lstWLOG01Response.errorCode !== undefined) {
      // TODO Error
      this.handleError(lstWLOG01Response.errorMessage);
      return;
    }

    console.log(lstWLOG01Response.items);

    /*const items = lstWLOG01Response.items.filter(item =>
      (item['IZVASH'] === '0' || item['IZVASH'] === '2') && item['IZZPFR'] === '0' && item['V_LFCD'] !== '1');*/


    const items = lstWLOG01Response.items.filter(item =>
      (item['IZVASH'] === '0' || item['IZVASH'] === '2') && item['IZZPFR'] === '0');

    const promisesLstWlLOG01_12 = [];
    items.forEach(element => {
      element.numeroOA = element.IBPUNO;
      element.article = element.IBITNO;
      element.libelle = element.MMITDS;
      element.lienFort = element.V_CDLF;
      element.reste = element.V_QRAL;
      element.quantite = 0;
      element.children = [];
      element.isFiltered = false;

      if (element.lienFort === '*') {
        promisesLstWlLOG01_12.push(this.lstWLOG01_12CMS100MI({
          SUNO: this.model.fournisseur,
          PUNO: element.numeroOA,
          ITNO: element.article
        })
          .then(resWLOG12 => {
            console.log(`resWLOG12 ${element.numeroOA} - ${element.article}`);
            if (resWLOG12.errorCode === undefined) {
              const children = resWLOG12.items;
              children.forEach(item => {
                item.numeroOA = item.IBPUNO;
                item.article = item.IBITNO;
                item.libelle = item.MMITDS;
                item.lienFort = item.V_CDLF;
                item.reste = item.V_QRAL;
                item.quantite = 0;
                item.isChild = true;
                item.isFiltered = false;
              })
              element.children.push(...children);
            }
            console.log(element);
          }))
      }


    });

    await Promise.all(promisesLstWlLOG01_12);

    console.log(items);

    debugger;

    this.dataSource.data = items;

    console.log(this.dataSource.data);
    this.table.renderRows();

    this.isLoading = false;


  }





  async lstWLOG01_01CMS100MI(params): Promise<any> {
    const request: IMIRequest = {
      program: 'CMS100MI',
      transaction: 'LstWLOG01_01',
      maxReturnedRecords: 0,
      record: {
        F_PUSL: '15',
        T_PUSL: '39',
        /*F_WHLO: params.F_WHLO,
        T_WHLO: params.T_WHLO,*/
        IBSUNO: params.SUNO
      }
    };

    const res = await this.executeAPI(request);

    if (res.errorMessage !== undefined) {
      console.log(`Appel ${request.program} ${request.transaction} KO:`, request, res);
    }
    return res;
  }
  

  async lstWLOG01_02CMS100MI(params): Promise<any> {
    const request: IMIRequest = {
      program: 'CMS100MI',
      transaction: 'LstWLOG01_02',
      maxReturnedRecords: 0,
      record: {
        F_RORC: '2',
        T_RORC: '2',
        J2USD1: `${params.USD1}`
      }
    };

    const res = await this.executeAPI(request);

    if (res.errorMessage !== undefined) {
      console.log(`Appel ${request.program} ${request.transaction} KO:`, request, res);
    }
    return res;
  }

  async lstWLOG01_03CMS100MI(params): Promise<any> {
    const request: IMIRequest = {
      program: 'CMS100MI',
      transaction: 'LstWLOG01_03',
      maxReturnedRecords: 0,
      record: {
        I7E0PA: params.E0PA,
        I7E065: 'MAN',
        I7E0IO: 'I'
      }
    };

    const res = await this.executeAPI(request);

    if (res.errorMessage !== undefined) {
      console.log(`Appel ${request.program} ${request.transaction} KO:`, request, res);
    }
    return res;
  }
/**
 * @api {Get} /LstWLOG01_12/ LstWLOG01_12
 * @apiDescription Créateur de ligne personnalisé MI
 * @apiParam {Number} F_PUSL          F_PUSL
 * @apiParam {Number} T_PUSL          T_PUSL
 * @apiParam {Number} IBSUNO          Supplier number
 * @apiParam {Number} IBPUNO          Purchase order number
 * @apiParam {Number} IBITNO          code article
 * @apiName LstWLOG01_12
 * @apiGroup CMS100MI
 * @apiVersion 0.2.0
 * @apiSuccess {String} result <code>ok</code> if everything went fine.
 * @apiError NoAccessRight Only authenticated user can access the data
 * @apiError (500 Internal Server Error) InternalServerError The server encountered an internal error
 */
  async lstWLOG01_12CMS100MI(params): Promise<any> {
    const request: IMIRequest = {
      program: 'CMS100MI',
      transaction: 'LstWLOG01_12',
      maxReturnedRecords: 0,
      record: {
        F_PUSL: '15',
        T_PUSL: '39',
        IBSUNO: params.SUNO,
        IBPUNO: params.PUNO,
        IBITNO: params.ITNO
      }
    };

    const res = await this.executeAPI(request);

    if (res.errorMessage !== undefined) {
      console.log(`Appel ${request.program} ${request.transaction} KO:`, request, res);
    }
    return res;
  }


 /**
 * @api {Get} /LstSuppliers/ LstSuppliers
 * @apiDescription Retrieve suppliers information
 * @apiName LstSuppliers
 * @apiGroup CRS620MI
 * @apiVersion 0.2.0
 * @apiSuccess {String}   SUNO            Supplier number
 * @apiSuccess {String}   SUNM            Supplier NAME
 * @apiSuccess {String} result <code>ok</code> if everything went fine.
 * @apiError NoAccessRight Only authenticated user can access the data
 * @apiError (500 Internal Server Error) InternalServerError The server encountered an internal error
 */
  async lstSuppliersCRS620MI(): Promise<any> {
    const request: IMIRequest = {
      program: 'CRS620MI',
      transaction: 'LstSuppliers',
      outputFields: ['SUNO', 'SUNM'],
      maxReturnedRecords: 0,
      record: {
        CONO: this.userContext.currentCompany
      }
    };

    const res = await this.executeAPI(request);

    if (res.errorMessage !== undefined) {
      console.log(`Appel ${request.program} ${request.transaction} KO:`, request, res);
    }
    return res;
  }
/**
 * @api {Get} /GetAlphaKPI/ GetAlphaKPI
 * @apiDescription Get single record
 * @apiName GetAlphaKPI
 * @apiGroup CUSEXTMI
 * @apiVersion 0.2.0
 * @apiSuccess {String} result <code>ok</code> if everything went fine.
 * @apiError NoAccessRight Only authenticated user can access the data
 * @apiError (500 Internal Server Error) InternalServerError The server encountered an internal error
 */
  async getAlphaKPICUSEXTMI(params): Promise<any> {
    const request: IMIRequest = {
      program: 'CUSEXTMI',
      transaction: 'GetAlphaKPI',
      maxReturnedRecords: 0,
      record: {
      }
    };

    // tslint:disable-next-line:forin
    for (const index in params) {
      request.record[index] = params[index];
    }

    const res = await this.executeAPI(request);

    if (res.errorMessage !== undefined) {
      console.log(`Appel ${request.program} ${request.transaction} KO:`, request, res);
    }
    return res;
  }
/**
 * @api {Post} /AddAlphaKPI/ AddAlphaKPI
 * @apiDescription Add record
 * @apiName AddAlphaKPI
 * @apiGroup CUSEXTMI
 * @apiVersion 0.1.0
 * @apiSuccess {String} result <code>ok</code> if everything went fine.
 * @apiError NoAccessRight Only authenticated user can access the data
 * @apiError (500 Internal Server Error) InternalServerError The server encountered an internal error
 */
  async addAlphaKPICUSEXTMI(params): Promise<any> {
    const request: IMIRequest = {
      program: 'CUSEXTMI',
      transaction: 'AddAlphaKPI',
      maxReturnedRecords: 0,
      record: {
      }
    };

    // tslint:disable-next-line:forin
    for (const index in params) {
      request.record[index] = params[index];
    }

    const res = await this.executeAPI(request);

    if (res.errorMessage !== undefined) {
      console.log(`Appel ${request.program} ${request.transaction} KO:`, request, res);
    }
    return res;
  }
/**
 * @api {Del} /DelAlphaKPI/ DelAlphaKPI
 * @apiDescription Delete record
 * @apiName DelAlphaKPI
 * @apiGroup CUSEXTMI
 * @apiVersion 0.1.0
 * @apiSuccess {String} result <code>ok</code> if everything went fine.
 * @apiError NoAccessRight Only authenticated user can access the data
 * @apiError (500 Internal Server Error) InternalServerError The server encountered an internal error
 */
  async delAlphaKPICUSEXTMI(params): Promise<any> {
    const request: IMIRequest = {
      program: 'CUSEXTMI',
      transaction: 'DelAlphaKPI',
      maxReturnedRecords: 0,
      record: {
      }
    };

    // tslint:disable-next-line:forin
    for (const index in params) {
      request.record[index] = params[index];
    }

    const res = await this.executeAPI(request);

    if (res.errorMessage !== undefined) {
      console.log(`Appel ${request.program} ${request.transaction} KO:`, request, res);
    }
    return res;
  }
/**
 * @api {Get} /LstRALOA/ LstRALOA
 * @apiDescription Créateur de ligne personnalisé MI
 * @apiParam {String} SUNO          Supplier number
 * @apiName LstRALOA
 * @apiGroup CMS100MI
 * @apiVersion 0.1.0
 * @apiSuccess {String} result <code>ok</code> if everything went fine.
 * @apiError NoAccessRight Only authenticated user can access the data
 * @apiError (500 Internal Server Error) InternalServerError The server encountered an internal error
 */
  async lstRALOACMS100MI(params): Promise<any> {
    const request: IMIRequest = {
      program: 'CMS100MI',
      transaction: 'LstRALOA',
      maxReturnedRecords: 0,
      record: {
        IBSUNO: params.SUNO
      }
    };

    const res = await this.executeAPI(request);

    if (res.errorMessage !== undefined) {
      console.log(`Appel ${request.program} ${request.transaction} KO:`, request, res);
    }
    return res;
  }

/**
 * @api {Get} /LstWLOG01_11/ LstWLOG01_11
 * @apiDescription Créateur de ligne personnalisé
 * @apiParam {String} SUNO          Supplier number
 * @apiParam {Number} PUNO          Purchase order number
 * @apiName LstWLOG01_11
 * @apiGroup CMS100MI
 * @apiVersion 0.1.0
 * @apiSuccess {String} result <code>ok</code> if everything went fine.
 * @apiError NoAccessRight Only authenticated user can access the data
 * @apiError (500 Internal Server Error) InternalServerError The server encountered an internal error
 */
  async lstWLOG01_11CMS100MI(params): Promise<any> {
    const request: IMIRequest = {
      program: 'CMS100MI',
      transaction: 'LstWLOG01_11',
      maxReturnedRecords: 0,
      record: {
        IBSUNO: params.SUNO,
        IBPUNO: params.PUNO
      }
    };

    const res = await this.executeAPI(request);

    if (res.errorMessage !== undefined) {
      console.log(`Appel ${request.program} ${request.transaction} KO:`, request, res);
    }
    return res;
  }
/**
 * @api {Get} /ASNdecharg/ ASNdecharg
 * @apiDescription Descendre l'ASN de déchargement
 * @apiParam {String} CONO          Société
 * @apiParam {Number} E0PA          code interface
 * @apiParam {String} USD1          N BL bloqué
 * @apiParam {String} DLIX          N Livraison
 * @apiName ASNdecharg
 * @apiGroup ZM3INTMI
 * @apiVersion 0.2.0
 * @apiSuccess {String} result <code>ok</code> if everything went fine.
 * @apiError NoAccessRight Only authenticated user can access the data
 * @apiError (500 Internal Server Error) InternalServerError The server encountered an internal error
 */
  async ASNdechargZM3INTMI(params): Promise<any> {
    const request: IMIRequest = {
      program: 'ZM3INTMI',
      transaction: 'ASNdecharg',
      maxReturnedRecords: 0,
      record: {
        CONO: this.userContext.currentCompany,
        E0PA: params.E0PA,
        USD1: params.USD1,
        DLIX: params.DLIX
      }
    };

    const res = await this.executeAPI(request);

    if (res.errorMessage !== undefined) {
      console.log(`Appel ${request.program} ${request.transaction} KO:`, request, res);
    }
    return res;
  }

/**
 * @api {Get} /Select/ Select
 * @apiDescription Data Export
 * @apiParam {String} SEPC          Separator character
 * @apiParam {Number} HDRS          Include Header 1 0 or blank
 * @apiParam {String} QERY          Query statement
 * @apiName Select
 * @apiGroup EXPORTMI
 * @apiVersion 0.2.0
 * @apiSuccess {String} result <code>ok</code> if everything went fine.
 * @apiError NoAccessRight Only authenticated user can access the data
 * @apiError (500 Internal Server Error) InternalServerError The server encountered an internal error
 */
  async selectEXPORTMICZZTAB(): Promise<any> {
    const request: IMIRequest = {
      program: 'EXPORTMI',
      transaction: 'Select',
      maxReturnedRecords: 0,
      record: {
        SEPC: ';',
        HDRS: '0',
        QERY: `CTZTKY from CZZTAB where CTCONO = ${this.userContext.currentCompany} and CTSTCO = 'WLOG01PACT'`
      }
    };

    const res = await this.executeAPI(request);

    if (res.errorMessage !== undefined) {
      console.log(`Appel ${request.program} ${request.transaction} KO:`, request, res);
    }
    return res;
  }
/**
 * @api {Get} /AdviseLine/ AdviseLine
 * @apiDescription Advise one line in a purchase order
 * @apiParam {Number} CONO          Company
 * @apiParam {String} PUNO          Purchase order number
 * @apiParam {Number} PNLI          Purchase order line
 * @apiParam {Number} PNLS          Purchase order line subnumber
 * @apiParam {String} SUNO          Supplier number
 * @apiParam {String} SUDO          Delivery not number
 * @apiParam {Number} ADQA          Advised quantity alternate U/M
 * @apiParam {String} PACT          Packaging
 * @apiParam {String} PACN          Package
 * @apiName AdviseLine
 * @apiGroup PPS001MI
 * @apiVersion 0.2.0
 * @apiSuccess {String} result <code>ok</code> if everything went fine.
 * @apiError NoAccessRight Only authenticated user can access the data
 * @apiError (500 Internal Server Error) InternalServerError The server encountered an internal error
 */
  async adviseLinePPS001MI(params): Promise<any> {
    const request: IMIRequest = {
      program: 'PPS001MI',
      transaction: 'AdviseLine',
      maxReturnedRecords: 0,
      record: {
        CONO: this.userContext.currentCompany,
        PUNO: params.PUNO,
        PNLI: params.PNLI,
        PNLS: params.PNLS,
        SUNO: params.SUNO,
        SUDO: params.SUDO,
        ADQA: params.ADQA,
        PACT: params.PACT,
        PACN: params.PACN
      }
    };

    const res = await this.executeAPI(request);

    if (res.errorMessage !== undefined) {
      console.log(`Appel ${request.program} ${request.transaction} KO:`, request, res);
    }
    return res;
  }

  async executeAPI(request): Promise<any> {
    try {
      const response = await this.miService.execute(request).toPromise();
      // console.log(response);
      return response;
    } catch (e) {
      // console.log(e);
      return e;
    }
  }

  handleError(msg: string): void {
    this.dialog.open(ErrorApiDialogComponent, {
      data: {
        error: msg
      }
    });
  }

  handleErrorAdviseLine(errors: any[]): any {
    return this.dialog.open(ErrorAdviseLineDialogComponent, {
      data: {
        errors: errors
      }
    });
  }


}
